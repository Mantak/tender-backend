# DealLink full stack developer assignment Back-end

Tender CRUD Backend API.

#### Requirements:
1. PHP >= 7.2.5 and all other extensions of Laravel 7 requirements
1. MariaDB 10.4 or SQLite
1. PHPUnit
1. Composer

#### Steps to install this repository
1. Clone this repo
1. Copy `.env.example` as `.env` and edit this new file for database configuration.
1. `composer update` to install packages for Laravel
1. `cd tender-backend && php artisan key:generate`
1. Do a database migration `php artisan migrate`
1. Do PHPUnit test with command `phpunit`
1. Create 4000 fake tender data: `php artisan tinker` and then `factory(App\Tender::class, 4000)->create();`
1. Start an application server if you're on your local computer `php artisan serve`. It will work on port 8000.
