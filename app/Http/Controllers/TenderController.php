<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tender;
use App\Http\Resources\Tender as TenderResource;
use Symfony\Component\HttpFoundation\Response;

class TenderController extends Controller
{
    public function index()
    {
        $tenders = Tender::orderBy('id', 'desc')->paginate(50);
        return TenderResource::collection($tenders);
    }

    public function store()
    {
        $tender = Tender::create($this->validateData());

        return (new TenderResource($tender))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Tender $tender)
    {
        return new TenderResource($tender);
    }

    public function update(Tender $tender)
    {
        $tender->update($this->validateData());

        return (new TenderResource($tender))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Tender $tender)
    {
        $tender->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    private function validateData()
    {
        return request()->validate([
            'title' => 'required|max:64',
            'description' => 'required|max:255',
        ]);
    }
}
