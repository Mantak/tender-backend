<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    protected $guarded = [];

    public function path()
    {
        return url('/tenders/' . $this->id);
    }
}
