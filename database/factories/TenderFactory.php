<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Tender::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->text
    ];
});
