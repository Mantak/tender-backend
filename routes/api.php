<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/tenders', 'TenderController@index');
Route::post('/tenders', 'TenderController@store');
Route::get('/tenders/{tender}', 'TenderController@show');
Route::patch('/tenders/{tender}', 'TenderController@update');
Route::delete('/tenders/{tender}', 'TenderController@destroy');