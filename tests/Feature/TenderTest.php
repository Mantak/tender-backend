<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Tender;
use Symfony\Component\HttpFoundation\Response;

class TenderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_list_of_tenders_can_be_fetched()
    {
        $tender = factory(Tender::class)->create();
        $response = $this->get('/api/tenders');

        // paginate count - 3, without - 1
        $response->assertJsonCount(3)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'tender_id' => $tender->id
                        ]
                    ]
                ]
            ]);
    }

    /** @test */
    public function a_tender_can_be_added()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/tenders', $this->data());

        $tender = Tender::first();

        // dd(json_decode($response->getContent()));

        $this->assertEquals('Test Title', $tender->title);
        $this->assertEquals('Test Description', $tender->description);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => [
                'tender_id' => $tender->id
            ],
            'links' => [
                'self' => $tender->path()
            ]
        ]);
    }

    /** @test */
    public function a_title_is_required()
    {
        $response = $this->post('/api/tenders', [
            'title' => '',
            'description' => 'Test Description'
        ]);

        $response->assertSessionHasErrors('title');
    }

    /** @test */
    public function fields_are_required()
    {
        collect(['title', 'description'])
            ->each(function($field) {
                $response = $this->post('/api/tenders',
                    array_merge($this->data(), [$field => '']));
        
                $response->assertSessionHasErrors($field);
                $this->assertCount(0, Tender::all());
            });
    }

    /** @test */
    public function a_tender_can_be_retrieved()
    {
        $tender = factory(Tender::class)->create();

        $response = $this->get('/api/tenders/' . $tender->id);

        $response->assertJson([
            'data' => [
                'tender_id' => $tender->id,
                'title' => $tender->title,
                'description' => $tender->description,
                'added' => $tender->created_at->diffForHumans(),
                'last_updated' => $tender->updated_at->diffForHumans(),
            ]
        ]);
    }

    /** @test */
    public function a_tender_can_be_patched()
    {
        $this->withoutExceptionHandling();

        $tender = factory(Tender::class)->create();

        $response = $this->patch('/api/tenders/' . $tender->id, $this->data());

        $tender = $tender->fresh();

        $this->assertEquals('Test Title', $tender->title);
        $this->assertEquals('Test Description', $tender->description);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'tender_id' => $tender->id
            ],
            'links' => [
                'self' => $tender->path()
            ]
        ]);
    }

    /** @test */
    public function a_tender_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $tender = factory(Tender::class)->create();

        $response = $this->delete('/api/tenders/' . $tender->id);

        $this->assertCount(0, Tender::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    protected function data()
    {
        return [
            'title' => 'Test Title',
            'description' => 'Test Description'
        ];
    }
}
